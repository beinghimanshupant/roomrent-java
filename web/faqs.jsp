<%-- 
    Document   : faqs
    Created on : Apr 28, 2018, 6:43:58 PM
    Author     : Himanshu Pant
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="header.jsp" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>RoomRent-FAQS</title>
    </head>
    <body>
        <br>
        <br>
        <div class="container">
        <div class="width75 floatRight">


      <!-- Gives the gradient block -->
      <div class="gradient">

        <a name="fluidity"></a>

        <p>


		  <a name="1" id="1"></a>
		  <ul>
            <li>What is RoomRent? </a></li>
				RoomRent is a secure online portal for apartment communities, fostering greater community interaction and offering locality information and a platform for common services. It opens the door to a better social life enabling the residents to interact, communicate and collaborate among each other.
          </ul>
<br \>
		  <a name="2" id="2"></a>
		  <ul>
            <li>Is RoomRent a social networking site?</a></li>
				RoomRent should not extends its value far beyond rather than being a socialRoomRent can be better called as local networking site. It is a social platform which let users share knowledge and resources. Apart from e-enabling the processes like urgent notification, maintenance payment, etc. it powers user with information about its locality. 
				
          </ul>
<br \>

		  <a name="3" id="3"></a>
		  <ul>
            <li>Is there any social hacking threat?</a></li>
				RoomRent is committed to safeguard users' privacy and protects their network from all sorts of social hacking. Your information can only be viewed by the people living in your apartment complex.
				
          </ul>
<br \>

		  <a name="4" id="4"></a>
		  <ul>
            <li>How can I join RoomRent? </a></li>
				Just follow the url links,post adds,view adds </ul>
<br \>
<br \>
			
<br \><br \><br \>

        </p>


      </div>
        </div>
           
    </body>
     <%@include file="footer.jsp" %>
</html>
