<%-- 
    Document   : search
    Created on : Jan 27, 2018, 7:13:59 PM
    Author     : Himanshu Pant
--%>

<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<div class="container">
    <div class="row">
        <ul class="thumbnails">
            <h3>Your Searched Result</h3>
            <p></p>
<%
     String driver = (String)application.getInitParameter("driver");
            String username=(String)application.getInitParameter("username");
            String url=(String)application.getInitParameter("url");
            String password=(String)application.getInitParameter("password");
String search=request.getParameter("search");
try{  
Class.forName(driver);  
 Connection con=DriverManager.getConnection(url,username,password);
 PreparedStatement ps=con.prepareStatement("select * from postadd where (state like ? or city like ? or title like ? or propertytype like ? )and verify = 1");
ps.setString(1,"%"+search+"%" );
ps.setString(2,"%"+search+"%" );
ps.setString(3,"%"+search+"%" );
ps.setString(4,"%"+search+"%" );
ResultSet rs=ps.executeQuery();
 boolean r = rs.next();
    if(!r){out.print("No match found!");}
    boolean b = rs.previous();
    while(rs.next()){
        String postid=rs.getString("postid");
         String title=rs.getString("title");
          String price=String.valueOf(rs.getInt("price"));
           String city=rs.getString("city");
            String state=rs.getString("state");
             String image=rs.getString("image1");
            
            
    
      %>  
      <div>
      <div class="col-md-4">
           <div class="thumbnail">
          <img src="<%=request.getContextPath()%>/images/<%= rs.getString("image1")%>" class="img-responsive" alt="hello" style="width:300px;height:200px">
 <div class="caption">
     <h4> <%=  title %></h4>
     <h5>Location :<%= rs.getString("city")%></h5>
     <h6> Price Rs<%=  price %> </h6>     
     <h6>Posted On<%=  rs.getDate("postdate") %></h6>
                        <p align="center"><a href="product.jsp?postid= <%= rs.getString("postid")%>" class="btn btn-primary btn-block">View</a>
                        </p>
                    </div>
           </div>  </div></div> <% }

                 }catch(Exception e){out.print(e);}
%>
</ul>
    </div>
</div>
    