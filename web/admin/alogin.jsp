<%-- 
    Document   : alogin
    Created on : Feb 23, 2018, 7:12:59 PM
    Author     : Himanshu Pant
--%>
<% 
if(session.getAttribute("adminemail")!=null)
{
response.sendRedirect("adminpanel.jsp");
}
%>
<jsp:include page="aheader.jsp"/>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link href="../files/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="../files/bootstrap/js/bootstrap.min.js"></script>
<script src="../files/bootstrap/js/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<form class="form-horizontal" action="aloginprocess.jsp" method="post">
<fieldset>

<!-- Form Name -->
<legend><center>Admin login</center></legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">UserName/email</label>  
  <div class="col-md-4">
  <input id="textinput" name="email" type="text" placeholder="Enter email/username" class="form-control input-md" required>
    
  </div>
</div>

<!-- Password input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="passwordinput">Password </label>
  <div class="col-md-4">
    <input id="passwordinput" name="password" type="password" placeholder="password" class="form-control input-md" required>
    <br>
     <input type="submit" class="btn btn-primary btn-lg btn-block login-button"value="Login"/>
  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="button1id"></label>
  <div class="col-md-8">
   
  </div>
</div>

</fieldset>
</form>
<div class="center-block" style="text-align: center">
<a href="../index.jsp">Home</a>
</div>
