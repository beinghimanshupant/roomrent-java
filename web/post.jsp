<%-- 
    Document   : post
    Created on : Dec 28, 2017, 12:58:29 PM
    Author     : Himanshu Pant
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="header.jsp" %>
<html>
<head>

</head>
  
  <body>
      
      <div class="container" style="margin-top:50px;">
          <form class="form-horizontal" method="post" action="postprocess.jsp" enctype="multipart/form-data">
          <fieldset>
    
          <div class="row">
            <div class="col-md-2 panel panel-heading">Contact Information</div>
          </div>

          <div class="row form-group">
              <label class="col-md-2 control-label" for="mobile">Contact No.</label>
            <div class="col-md-3">
    <div class="input-group">
			<span class="input-group-addon">
			</span>
              <input id="mobile" maxlength="10" name="mobile"  pattern="^\d{10}$" placeholder="Enter digit mobile number" class="form-control input-md ac_mobile" type="text" required  >
		
            </div>
	</div>
            <label class="col-md-1 control-label" for="sms">I am</label>
            <div class="col-md-3">
		<div class="input-group">
			<span class="input-group-addon">
			
			</span>
              <select id="sms" name="relate" class="form-control input-md">
                <option value="Owner">Owner</option>
                <option value="Representative">Representative</option>
                <option value="other">Other</option>
                
              </select>
            </div>
		</div>
          </div>


          <div class="row form-group">
            <label class="col-md-2 control-label" for="first_name">Name</label>  
            <div class="col-md-2">
			<div class="input-group">
			<span class="input-group-addon">

			</span>
                            <input id="first_name" name="name" placeholder="" class="form-control input-md" type="text" maxlength="25">
            </div></div>
             <label class="col-md-2 control-label" for="first_name">Email</label>  
            <div class="col-md-2">
			<div class="input-group">
			<span class="input-group-addon">

			</span>
              <input id="email" name="email" placeholder="" class="form-control input-md" type="email" maxlength="40">
            </div></div>


          </div>
          <div class="row">
            <div class="col-md-2 panel panel-heading">Property Information</div>
            <div class="col-md-4 panel panel-heading" style="display:none; color:red" id="address_error"></div>
              </div>
          <div class="row form-group">
            <label class="col-md-2 control-label" for="property_type">Property For</label>
            <div class="col-md-4">
              <label class="radio-inline"><input type="radio" name="propertyfor" value="Rent" checked>Rent</label>
              <label class="radio-inline"><input type="radio" name="propertyfor" value="PG">PG</label>
            </div>
            
               <label class="col-md-1 control-label" for="sms">Property Type</label>
            <div class="col-md-3">
		<div class="input-group">
			<span class="input-group-addon">
			</span>
              <select id="sms" name="propertytype" class="form-control input-md">
                <option value="Residental">Residential</option>
                <option value="Multistorey">Multistorey</option>
                <option value="Villa">Villa</option>
                <option value="Penthous">Penthouse</option>
                  <option value="Studio Apartment">Studio Apartment</option>
                <option value="Commercial Office">Commercial Office</option>
                <option value="Commercial Shop">Commercial Shop</option>
                <option value="Showroom">Showroom</option>
              </select>
            </div>
		</div>
              </div>
              
          <div class="row">
            <div class="col-md-2 panel panel-heading">Locality Information</div>
            <div class="col-md-4 panel panel-heading" style="display:none; color:red" id="address_error"></div>
          </div>

          <div class="row form-group">
            <label class="col-md-2 control-label" for="village">City/Town</label>
            <div class="col-md-2">
		<div class="input-group">
			<span class="input-group-addon">
			</span>

              <input id="village" name="city" placeholder="" class="form-control input-md ac_village" required type="text" maxlength="25">
            </div> </div>

            <label class="col-md-1 control-label" for="state">State</label>
            <div class="col-md-2">
		<div class="input-group">
			<span class="input-group-addon">
			
			</span>

              <select id="state" name="state" placeholder="" class="form-control input-md ac_state" required>
           <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
<option value="Andhra Pradesh">Andhra Pradesh</option>
<option value="Arunachal Pradesh">Arunachal Pradesh</option>
<option value="Assam">Assam</option>
<option value="Bihar">Bihar</option>
<option value="Chandigarh">Chandigarh</option>
<option value="Chhattisgarh">Chhattisgarh</option>
<option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
<option value="Daman and Diu">Daman and Diu</option>
<option value="Delhi">Delhi</option>
<option value="Goa">Goa</option>
<option value="Gujarat">Gujarat</option>
<option value="Haryana">Haryana</option>
<option value="Himachal Pradesh">Himachal Pradesh</option>
<option value="Jammu and Kashmir">Jammu and Kashmir</option>
<option value="Jharkhand">Jharkhand</option>
<option value="Karnataka">Karnataka</option>
<option value="Kerala">Kerala</option>
<option value="Lakshadweep">Lakshadweep</option>
<option value="Madhya Pradesh">Madhya Pradesh</option>
<option value="Maharashtra">Maharashtra</option>
<option value="Manipur">Manipur</option>
<option value="Meghalaya">Meghalaya</option>
<option value="Mizoram">Mizoram</option>
<option value="Nagaland">Nagaland</option>
<option value="Orissa">Orissa</option>
<option value="Pondicherry">Pondicherry</option>
<option value="Punjab">Punjab</option>
<option value="Rajasthan">Rajasthan</option>
<option value="Sikkim">Sikkim</option>
<option value="Tamil Nadu">Tamil Nadu</option>
<option value="Tripura">Tripura</option>
<option value="Uttarakhand">Uttarakhand</option>
<option value="Uttar Pradesh">Uttar Pradesh</option>
<option value="West Bengal">West Bengal</option>
              </select>
                </div></div>

            <label class="col-md-1 control-label" for="country">Country</label>
            <div class="col-md-2">
		<div class="input-group">
			<span class="input-group-addon">
			</span>

              <input id="district" name="country" placeholder="INDIA" class="form-control input-md ac_district" required type="text" disabled="true">
            </div>
		</div>
          </div>
          <div class="row form-group">
            <label class="col-md-2 control-label" for="street_address">Address Line 1</label>
            <div class="col-md-5">
		<div class="input-group">
			<span class="input-group-addon">
			</span>
              <textarea class="form-control" id="street_address" name="address" placeholder="Addess Details..."></textarea>
            </div>
	</div>
          </div>
  <div class="row">
            <div class="col-md-2 panel panel-heading">Availability</div>
            <div class="col-md-4 panel panel-heading" style="display:none; color:red" id="address_error"></div>
          </div>

          <div class="row form-group">
            <label class="col-md-2 control-label" for="">Available</label>
             <div class="col-md-4">
              <label class="radio-inline"><input type="radio" name="available" value="Sessional" checked>Sessional</label>
              <label class="radio-inline"><input type="radio" name="available" value="Full Time">Full Time</label>
              <label class="radio-inline"><input type="radio" name="available" value="One Year">One Year</label>
            </div>
              <label class="col-md-2 control-label" for="Availabefrom">Available From</label>
               <div class="col-md-4">
                    <label>
                        <input class="form-control input-md" type="date" name="availabledate" value="" ></label>
              </div>
        
          </div>
<div class="row form-group">
     <label class="col-md-2 control-label" for="sms">Age of Construction</label>
            <div class="col-md-3">
		<div class="input-group">
			<span class="input-group-addon">
			
			</span>
              <select id="sms" name="age" class="form-control input-md">
                <option value="New">New</option>
                <option value="Within 5 Years">Within 5 Years</option>
                <option value="5-10 Years">5-10 Years</option>
                <option value="10-15 Years">10-15 Years</option>
                <option value="15-20 Years">15-20 Years</option>
                <option value="Above 20 Years">Above 20 Years</option> 
              </select>
            </div>
		</div>
 </div>
          <div class="row">
            <div class="col-md-2 panel panel-heading">Other Details</div>
            <div class="col-md-4 panel panel-heading" style="display:none; color:red" id="address_error"></div>
          </div>

          <div class="row form-group">
            <label class="col-md-2 control-label" for="title">Title</label>
            <div class="col-md-2">
		<div class="input-group">
			<span class="input-group-addon">
			</span>

              <input id="title" name="title" placeholder="" class="form-control input-md ac_village" required type="text">
            </div> </div>

            <label class="col-md-2 control-label" for="Price">Price</label>
            <div class="col-md-2">
		<div class="input-group">
			<span class="input-group-addon">
			</span>

              <input id="price" name="price" placeholder="" class="form-control input-md ac_state" required type="number">
            </div></div>

            <label class="col-md-2 control-label" for="district">Carpet Area(in sq.ft)</label>
            <div class="col-md-2">
		<div class="input-group">
			<span class="input-group-addon">
			</span>
              <input id="carpet" name="carpetarea" placeholder="" class="form-control input-md ac_district" required="" type="number">
            </div>
    
		</div>
          </div>
             <div class="row form-group">
             <label class="col-md-2 control-label" for="Bedroom">No. of Bedrooms</label>
            <div class="col-md-2">
		<div class="input-group">
			<span class="input-group-addon">
			</span>
              <input id="carpet" name="bedroom" placeholder="" class="form-control input-md ac_district" required="" type="number">
            </div>
          </div> 
           <label class="col-md-2 control-label" for="district">Kitchen</label>
            <div class="col-md-2">
	 <label class="radio-inline"><input type="radio" name="kitchen" value="Yes" checked>Yes</label>
              <label class="radio-inline"><input type="radio" name="kitchen" value="No">No</label>
          </div>
                 <label class="col-md-2 control-label" for="bathroom">No. of Bathrooms</label>
            <div class="col-md-2">
		<div class="input-group">
			<span class="input-group-addon">
			</span>
              <input id="carpet" name="bathroom" placeholder="" class="form-control input-md ac_district" required="" type="number">
            </div>
          </div>
              </div>
              <div class="row">
            <div class="col-md-3 panel panel-heading">Additional Charges(if applicable)</div>
            <div class="col-md-4 panel panel-heading" style="display:none; color:red" id="address_error"></div>
          </div>

          <div class="row form-group">
            <label class="col-md-2 control-label" for="">Covered Vehicle Parking</label>
             <div class="col-md-2">
              <label class="radio-inline"><input type="radio" name="vehicle" value="Yes" checked>Yes</label>
              <label class="radio-inline"><input type="radio" name="vehicle" value="No">No</label>
            </div>
              <label class="col-md-2 control-label" for="monthly">Monthly Maintenance</label>
                <div class="col-md-2">
		<div class="input-group">
			<span class="input-group-addon">
			</span>
              <input id="carpet" name="monthly" placeholder="" class="form-control input-md ac_district" required="" type="number">
            </div>
          </div>
              <label class="col-md-2 control-label" for="security">Security Deposit</label>
                <div class="col-md-2">
		<div class="input-group">
			<span class="input-group-addon">
			</span>
              <input id="carpet" name="security" placeholder="" class="form-control input-md ac_district" required="" type="number">
            </div>
          </div>
        
          </div>
               <div class="row form-group">
            <label class="col-md-2 control-label" for="">Water and Electricity Bill Included?</label>
             <div class="col-md-2">
              <label class="radio-inline"><input type="radio" name="electricity" value="Yes" checked>Yes</label>
              <label class="radio-inline"><input type="radio" name="electricity" value="No">No</label>
            </div>
              </div>
                <div class="row">
            <div class="col-md-3 panel panel-heading">Upload Images</div>
            <div class="col-md-4 panel panel-heading" style="display:none; color:red" id="image_error"></div>
          </div>
               <div class="row form-group">
            <label class="col-md-2 control-label" for="Price">Image 1</label>
            <div class="col-md-2">
		<div class="input-group">
			

              <input id="image1" name="image1" placeholder="" required type="file">
            </div></div>
            <label class="col-md-2 control-label" for="Price">Image 2</label>
            <div class="col-md-2">
		<div class="input-group">
			

              <input id="image2" name="image2" placeholder="" required type="file">
            </div></div>
            <label class="col-md-2 control-label" for="Price">Image 3</label>
            <div class="col-md-2">
		<div class="input-group">
			

              <input id="image3" name="image3" placeholder=""  required type="file">
            </div></div>
            
               </div>
               <div class="row">
            <div class="col-md-2 panel panel-heading">Description</div>
            <div class="col-md-4 panel panel-heading" style="display:none; color:red" id="address_error"></div>
          </div>
            <div class="col-md-5">
		<div class="input-group">
			<span class="input-group-addon">
			</span>
              <textarea class="form-control" id="detail" name="description" placeholder=" other descriptions like balconies,furnishing states, pet allowed , terms and conditions etc..." maxlength="100"></textarea>
            </div>
	</div>

    
          <div class="form-group row">
            <div class="col-md-8 text-center">
              <button id="save" name="save" class="btn btn-large btn-success"> Save Information</button>
            </div>
          </div>
          </fieldset>
        </form>
      </div>

</body>

</html>
<%@include file="footer.jsp" %>