<%-- 
    Document   : inbox
    Created on : Mar 14, 2018, 11:26:01 PM
    Author     : Himanshu Pant
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="sellerpanel.jsp" %>
<link href="../files/main.css" rel="stylesheet" type="text/css"/>
<div class="container">
 <div class="mail-box">
                  <aside class="sm-side">
                      <div class="user-head">    
                      </div>
                      <ul class="inbox-nav inbox-divider">
                          <li class="active">
                              <a href="#"><i class="fa fa-inbox"></i> Inbox </a>
                          </li>
                          <li>
                              <a href="#"><i class="fa fa-envelope-o"></i> Sent Mail</a>
                          </li>
                      </ul>
                  </aside>
                  <aside class="lg-side">
                      <div class="inbox-body">
                         <div class="mail-option"> 
                          <table class="table table-inbox table-hover">
                            <tbody>
                                <tr>
                                    <td class="view-message  dont-show">PHPClass</td>
                                    <td class="view-message "> <a href="#"> Added a new class: Login Class Fast Site </a></td>
                                  <td class="view-message  text-right">9:27 AM</td>
                            
                              </tr>   
                          </tbody>
                          </table>
                      </div>
                  </aside>
              </div>
</div>
