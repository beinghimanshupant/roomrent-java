<%-- 
    Document   : display
    Created on : Jan 22, 2018, 8:56:27 PM
    Author     : Himanshu Pant
--%>
<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="header.jsp" %>
 <%@include file="filter.jsp" %>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="files/main.css">
<script type="text/javascript" src="files/view.js"></script>
<div class="container">
    <div class="row">
        <ul class="thumbnails">
            <div style="width:50%; alignment-adjust: auto;">
     
           <form method="post" action="#" name="Form">
<label for="sortby" >Sort By</label>
<select name="sort" onchange="document.Form.submit()">
             
            <option value="Popular">Popular</option>
            <option value="postdate ASC">Posted On(Oldest First)</option>
            <option value="postdate desc">Posted On(Recent First)</option>
            <option value="price desc">Price(High to Low)</option>
            <option value="price asc">Price(Low to High)</option>
        </select>
    </form>
  
            </div>
   <%!
public int nullIntconv(String str)
{
int conv=0;
if(str==null)
{
str="0";
}
else if((str.trim()).equals("null"))
{
str="0";
}
else if(str.equals(""))
{
str="0";
}
try{
conv=Integer.parseInt(str);
}
catch(Exception e)
{
System.out.print("error"+e);
}
return conv;
}
%>     
            
            
<% 
    String s = null;
    String driver = (String)application.getInitParameter("driver");
            String username=(String)application.getInitParameter("username");
            String url=(String)application.getInitParameter("url");
            String password=(String)application.getInitParameter("password");
    String sql=null;
    String sort=request.getParameter("sort");
    //out.print(sort);
    Statement st;
   ResultSet rsRowCnt = null;
   PreparedStatement psRowCnt=null;
   int iShowRows=5; // Number of records show on per page
int iTotalSearchRecords=10; // Number of pages index shown

int iTotalRows=nullIntconv(request.getParameter("iTotalRows"));
int iTotalPages=nullIntconv(request.getParameter("iTotalPages"));
int iPageNo=nullIntconv(request.getParameter("iPageNo"));
int cPageNo=nullIntconv(request.getParameter(""));
int iStartResultNo=0;
int iEndResultNo=0;

if(iPageNo==0)
{
iPageNo=0;
}
else{
iPageNo=((iPageNo-1)*iShowRows);
}
     sql="SELECT SQL_CALC_FOUND_ROWS * FROM postadd where verify = 1 order by "+ sort+" limit "+iPageNo+","+iShowRows+"";
    
    try{  
Class.forName(driver);  
 Connection con=DriverManager.getConnection(url,username,password);
  st = con.createStatement();
    ResultSet rs=st.executeQuery(sql);
    //// this will count total number of rows
String sqlRowCnt="SELECT FOUND_ROWS() as cnt";
psRowCnt=con.prepareStatement(sqlRowCnt);
rsRowCnt=psRowCnt.executeQuery();

if(rsRowCnt.next())
{
iTotalRows=rsRowCnt.getInt("cnt");
} %>
<form name=?frm?>
<input type="hidden" name="iPageNo" value="<%=iPageNo%>">
<input type="hidden" name="cPageNo" value="<%=cPageNo%>">
<input type="hidden" name="iShowRows" value="<%=iShowRows%>">
 <%   while(rs.next()){
       
         String title=rs.getString("title");
          String price=String.valueOf(rs.getInt("price"));
          //String email = rs.getString("email");
      %>  
      <div>
      <div class="col-md-4">
           <div class="thumbnail">
          <img src="<%=request.getContextPath()%>/images/<%= rs.getString("image1")%>" class="img-responsive" alt="hello" style="width:300px;height:200px">
 <div class="caption">
     <div style="height: 150px">
     <h4> <%=  title %>, <%= rs.getInt("bedroom")%>BHK</h4>
     <h5>Location :<%= rs.getString("city")%></h5>
     <h6> Price Rs<%=  price %> </h6>     
     <h6>Posted On<%=  rs.getDate("postdate") %></h6>
     <h6>Available as <%= rs.getString("propertyfor")%> from <%= rs.getString("availabledate")%></h6>
                        <p align="center"><a href="product.jsp?postid=<%= rs.getInt("postid")%>" class="btn btn-primary btn-block">View</a>
                        </p>
                    </div>
 </div>  </div></div></div>
 <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        </div>
        <div class="modal-body">
          <img src="" class="enlargeImageModalSource" style="width: 100%;">
        </div>
      </div>
    </div>
</div>
            <% }
}catch(Exception e){
out.print(e);}
    %>
</form>
        </ul>
    </div>
</div>
    <% 
//// calculate next record start record and end record
try{
if(iTotalRows<(iPageNo+iShowRows))
{
iEndResultNo=iTotalRows;
}
else
{
iEndResultNo=(iPageNo+iShowRows);
}

iStartResultNo=(iPageNo+1);
iTotalPages=((int)(Math.ceil((double)iTotalRows/iShowRows)));

}
catch(Exception e)
{
out.print(e);
}

%>

<div style="text-align: center; font-size: large">
<%
        //// index of pages 
         
        int i=0;
        int cPage=0;
        if(iTotalRows!=0)
        {
        cPage=((int)(Math.ceil((double)iEndResultNo/(iTotalSearchRecords*iShowRows))));
         
        for(i=((cPage*iTotalSearchRecords)-(iTotalSearchRecords-1));i<=(cPage*iTotalSearchRecords);i++)
        {
          if(i==((iPageNo/iShowRows)+1))
          {
          %>
          <a href="display.jsp?iPageNo=<%=i%>" style="cursor:pointer;color: red" class="pagination pagination-centered"><b><%=i%></b></a>
          <%
          }
          else if(i<=iTotalPages)
          {
          %>
          <a href="display.jsp?iPageNo=<%=i%>" class="pagination pagination-centered"><%=i%></a>
          <% 
          }
        }
        }
      %>
<b>Adds <%=iStartResultNo%> - <%=iEndResultNo%></b>
</div>
</body>
</html>

 <%@include file="footer.jsp" %>