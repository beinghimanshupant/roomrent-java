<%-- 
    Document   : wishlist
    Created on : Mar 17, 2018, 10:16:59 PM
    Author     : Himanshu Pant
--%>

<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"/>
 <div class="site-panel"style="margin-top:110px">
    <div class="container">
        <div class="row">
                <div class="col-md-9 ">
                    <div class= "content-box well">
                     <legend>My Wishlist</legend>
                     
                       
<%
    String email = (session.getAttribute("username")).toString();
  // String email="abhi12@gmail.com";
    String driver = (String)application.getInitParameter("driver");
            String username=(String)application.getInitParameter("username");
            String url=(String)application.getInitParameter("url");
            String password=(String)application.getInitParameter("password");
    //String postid=request.getParameter("postid");
     try{  
Class.forName(driver);  
 Connection con=DriverManager.getConnection(url,username,password);
 PreparedStatement ps = con.prepareStatement("select distinct * from wishlist where email=?");
    ps.setString(1,email);
ResultSet rs=ps.executeQuery();
boolean b = rs.next();
if(!b){
out.print("Your wish list is empty.");
}
boolean r = rs.previous();
 while(rs.next()){ %>
   <div class="row ">
                             <div class="list-car-block clearfix">
                             <div class="col-md-4">
                <div class="list-car-img">
 <img class="img-responsive" src="<%=request.getContextPath()%>/images/<%= rs.getString("image")%>">
                </div>
            </div>
                             <div class="col-md-8">
                <div class="list-car-title clearfix">
                    <h4><%= rs.getString("title") %> </h4>
                    <hr>
                </div>
                <div class="car-details clearfix">
                  <ul class="list-inline" >
                      <li><i></i> Price <li>| </li><small> Rs <%=rs.getString("price")  %></small></li>
                  <li><i></i> Location  <li> | </li> <small> <%=rs.getString("location")  %></small></li>
                    <li><i></i> Bedroom  <li>| </li><small> <%=rs.getString("bedroom")  %></small></li>
                    
                 
                  </ul>    
    
            </div>
               
                <div class="car-bottom">
                    <ul class="list-inline">
                        <li><a href="deletewishlist.jsp?id=<%= rs.getInt("postid")%>">Remove</a></li>
                  
                    </ul>
                </div>
            </div>
                            </div>
                         </div>
 <%
 }
 
     }catch(Exception e){
     }
    %>
</div>
    </div>
        </div>
    </div>
 </div>