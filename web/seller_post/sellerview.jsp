<%-- 
    Document   : productttt
    Created on : Feb 16, 2018, 3:38:09 PM
    Author     : Himanshu Pant
--%>

<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!DOCTYPE html>
<%@ include file="header.jsp" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>RoomRent</title>
        <script type="text/javascript" src="../files/productjs.js">
        
        </script>
        <link href="../files/productcss.css" rel="stylesheet" type="text/css" media="all" />
    </head>
    <body>
        <%
            String driver = (String)application.getInitParameter("driver");
            String username=(String)application.getInitParameter("username");
            String url=(String)application.getInitParameter("url");
            String password=(String)application.getInitParameter("password");
    String postid=request.getParameter("postid");
    boolean status=false;
    try{  
//Class.forName("com.mysql.jdbc.Driver");  
//Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/RoomRent","root","");
 Class.forName(driver);  
 Connection con=DriverManager.getConnection(url,username,password);
 PreparedStatement ps = con.prepareStatement("select * from postadd where postid=?");
    ps.setString(1,postid);
ResultSet rs=ps.executeQuery();
 while(rs.next()){ 
         String title=rs.getString("title"); %>
        <div class="container">
            <div class="row">
                    <div class="col-sm-6">
                    <img style="width: 400px; height: 500px" src="<%=request.getContextPath()%>/images/<%= rs.getString("image1")%>"class="img-responsive" alt="<%=title%>"/>
                </div>
                <div class="col-xs-6 row form-group" style="border:0px solid gray">
                    <!-- title  -->
                    <h3>  <%=rs.getString("title") %></h3>
                    <!-- square feet -->
                    <h4 class="title-price"><small>Carpet Area(Sq.Ft)</small></h4>
                    <h3 style="margin-top:0px;"> <%=rs.getString("carpetarea") %></h3>
                     <!-- Price -->
                    <h4 class="title-price"><small>Price</small></h4>
                    <h3 style="margin-top:0px;">₹<%=rs.getString("price") %></h3>
                 
                  <h4 class="title-price"><small>Configuration</small></h4>
                 
                  <div> <h3 style="margin-top:0px;" > <%=rs.getInt("bedroom") %>Bedrooms,<%=rs.getInt("bedroom") %>Bathrooms Kitchen:<%=rs.getString("kitchen")%></h3> </div>
               
                     <h4 class="title-price"><small>Address</small></h4>
                    <h3  style="margin-top:0px;"><%=rs.getString("address") %></h3> 
                                                 
                                         
                   
                          </div>
                <div class="col-xs-12">
         
                    <div style="width:100%;border-top:1px solid silver">
                       <div class="tab">
  <button class="tablinks button1" onclick="openCity(event, 'London')" >Property Information</button>
  <button class="tablinks button1" onclick="openCity(event, 'Paris')">Additional Charges</button>
  <button class="tablinks button1" onclick="openCity(event, 'Tokyo')">Contact Information</button>
</div>

<div id="London" class="tabcontent">
  <div class="row form-group">
               <label class="col-md-2 control-label" for="sms">Property Type</label>
            <div class="col-md-1">
		<div>
			<%=rs.getString("propertytype") %>
            </div>
		</div>
               <label class="col-md-2 control-label" for="village">Available As:</label>
            <div class="col-md-1">
		<div class="input-group">
              <%=rs.getString("available") %>
            </div> </div>
            <label class="col-md-2 control-label" for="sms">Available From:</label>
            <div>
		<div>
			<%=rs.getString("availabledate") %>
            </div>
		</div>
              </div>
            <div class="row form-group">
     <label class="col-md-2 control-label" for="sms">Age of Construction</label>
            <div class="col-md-3">
		<%=rs.getString("age") %>
		</div>
 </div>
                 <div class="row">
            <div class="col-md-2 panel panel-heading">Description</div>
            <div class="col-md-4 panel panel-heading" style="display:none; color:red" id="address_error"></div>
          </div>
            <div>
		<div class="input-group">
			<%=rs.getString("description") %>
            </div>
	</div>
             
     
    
    
</div>

<div id="Paris" class="tabcontent">
  
     <div class="row form-group">
            <label class="col-md-2 control-label" for="">Covered Vehicle Parking</label>
             <div class="col-md-2">
              <%=rs.getString("vehicle") %>
            </div>
              <label class="col-md-2 control-label" for="village">Monthly Maintenance</label>
                <div class="col-md-2">
		<div class="input-group">
			₹<%=rs.getString("monthly") %>
            </div>
          </div>
              <label class="col-md-2 control-label" for="village">Security Deposit</label>
                <div class="col-md-2">
		₹<%=rs.getString("security") %>
          </div>
        
          </div>
               <div class="row form-group">
            <label class="col-md-2 control-label" for="">Water and Electricity Bill Included?</label>
             <div class="col-md-2">
            <%=rs.getString("electricity") %>
            </div>
              </div>
</div>

<div id="Tokyo" class="tabcontent">
          <div class="row form-group">
            <label class="col-md-2 control-label" for="first_name">Name</label>  
            <div class="col-md-2">
			<div class="input-group">
			<%=rs.getString("name") %> (<%=rs.getString("relate") %>)
            </div></div>
            <label class="col-md-2 control-label" for="ccmobile">Contact No.</label>
            <div class="col-md-3">
    <div class="input-group">
			<%=rs.getLong("mobile") %>
		
            </div>
	</div>

          </div>
                         <div class="row">
            <div class="col-md-2 panel panel-heading">Contact Address</div>
            <div class="col-md-4 panel panel-heading" style="display:none; color:red" id="address_error"></div>
          </div>

        
          <div class="row form-group">
            <label class="col-md-3 control-label" for="street_address">Address Line 1</label>
            <div class="col-md-5">
		<div class="input-group">
			<%=rs.getString("address") %>
            </div>
            <div class="input-group">
			<%=rs.getString("state") %>
            </div>
	</div>
          </div>
         
</div>
             </div>  
        </div>
        </div>
        </div>
            <div id="carousel-example-generic" class="carousel slide container" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>
            <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
         <center> 
      <img src="<%=request.getContextPath()%>/images/<%= rs.getString("image1")%>" class="img-responsive" alt="hello" style="width: 850px; height: 500px">
            </center>  
    </div>
      <div class="item">
          <center>
      <img src="<%=request.getContextPath()%>/images/<%= rs.getString("image2")%>" class="img-responsive" alt="hello" style="width: 850px; height: 500px" >
          </center>
      </div>
    <div class="item">
        <center>
      <img src="<%=request.getContextPath()%>/images/<%= rs.getString("image3")%>" class="img-responsive" alt="hello" style="width: 850px; height: 500px" >
        </center>
    </div>
   
        </div>
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
                </div>
        <div style="text-align: center">
            <a href="sellerpost.jsp">Back</a>
        </div>     
 <% } 
}catch(Exception e){
out.print(e);}
        
        %>
      