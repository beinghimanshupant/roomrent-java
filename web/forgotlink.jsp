<%-- 
    Document   : forgotlink
    Created on : May 4, 2018, 4:56:36 PM
    Author     : Himanshu Pant
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="header.jsp" %>
<!DOCTYPE html>
<html lang="en">
    <head> 
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Website CSS style -->
		<link rel="stylesheet" type="text/css" href="files/main.css">
		<script type="text/javascript">
    function confirmPass() {
        var pass = document.getElementById("password").value;
        var confPass = document.getElementById("confirm").value;
        if(pass != confPass) {
            alert('Wrong confirm password !');
            return false;
        }
    }
</script>
		
	</head>
	<body>
		<div class="container">
			<div class="row main">
				<div class="panel-heading">
	               <div class="panel-title text-center">
	               		<h3 class="title">Welcome! Change password</h3>
                                
	               		<hr />
	               	</div>
	            </div> 
				<div class="main-login main-center">
                                    <form class="form-horizontal" method="post" action="changepassword.jsp" onsubmit="return confirmPass()">
						<h6>All fields are required</h6>
                                                <input type="hidden" name="email" value="<%=request.getParameter("email")%>">
						<div class="form-group">
							<label for="password" class="cols-sm-2 control-label">Password</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="password" id="password" maxlength="15" required placeholder="Enter your Password"/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="confirm" class="cols-sm-2 control-label">Confirm Password</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="confirm" id="confirm"  placeholder="Confirm your Password"/>
								</div>
							</div>
						</div>

						<div class="form-group ">
                                                    <input type="submit" class="btn btn-primary btn-lg btn-block login-button"value="Register"/>
						</div>
				</div>
			</div>
		</div>
	</body>
</html>
