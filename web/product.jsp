<%-- 
    Document   : productttt
    Created on : Feb 16, 2018, 3:38:09 PM
    Author     : Himanshu Pant
--%>

<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!DOCTYPE html>
<%@ include file="header.jsp" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>RoomRent</title>
        <script type="text/javascript" src="files/productjs.js">
        
        </script>
        <link href="files/productcss.css" rel="stylesheet" type="text/css" media="all" />
    </head>
    <body>
        <%
            String driver = (String)application.getInitParameter("driver");
            String username=(String)application.getInitParameter("username");
            String url=(String)application.getInitParameter("url");
            String password=(String)application.getInitParameter("password");
    String postid=request.getParameter("postid");
    boolean status=false;
    try{  
//Class.forName("com.mysql.jdbc.Driver");  
//Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/RoomRent","root","");
 Class.forName(driver);  
 Connection con=DriverManager.getConnection(url,username,password);
 String s = null;
 PreparedStatement ps = con.prepareStatement("select * from postadd where postid=?");
    ps.setString(1,postid);
ResultSet rs=ps.executeQuery();
 while(rs.next()){ 
     
         String title=rs.getString("title"); %>
         <% 
              PreparedStatement p = con.prepareStatement("select status from seller where email=?");
 p.setString(1, rs.getString("email"));
 ResultSet r =p.executeQuery();
 while(r.next()){
 s = r.getString("status");
 }
              %>
        <div class="container">
            <div class="row">
                    <div class="col-sm-6">
                    <img style="width: 500px; height: 400px" src="<%=request.getContextPath()%>/images/<%= rs.getString("image1")%>"class="img-responsive" alt="<%=title%>"/>
                </div>
                <div class="col-xs-6 row form-group" style="border:0px solid gray">
                    <!-- title  -->
                    <h3>  <%=rs.getString("title") %><% if(s.equals("success")) {out.print("&#10004;");}%></h3>
                    <!-- square feet -->
                    <h4 class="title-price"><small>Carpet Area(Sq.Ft)</small></h4>
                    <h3 style="margin-top:0px;"> <%=rs.getString("carpetarea") %></h3>
                     <!-- Price -->
                    <h4 class="title-price"><small>Price</small></h4>
                    <h3 style="margin-top:0px;">₹<%=rs.getString("price") %></h3>
                 
                  <h4 class="title-price"><small>Configuration</small></h4>
                 
                  <div> <h3 style="margin-top:0px;" > <%=rs.getInt("bedroom") %>Bedrooms,<%=rs.getInt("bedroom") %>Bathrooms Kitchen:<%=rs.getString("kitchen")%></h3> </div>
               
                     <h4 class="title-price"><small>Address</small></h4>
                    <h3  style="margin-top:0px;"><%=rs.getString("address") %></h3> 
                                                 
                         <div>
                             <form action="addwishlist.jsp" method="get">
                               <input type="hidden" name="postid" value="<%=postid %>">
                        <input type="hidden" name="title" value="<%= rs.getString("title") %>">
                        <input type="hidden" name="image" value="<%= rs.getString("image1") %>">
                        <input type="hidden" name="price" value="<%= rs.getString("price") %>">
                        <input type="hidden" name="bedroom" value="<%= rs.getString("bedroom") %>">
<input type="hidden" name="location" value="<%= rs.getString("city") %>">
                         <span class="input-group-btn">
                <button class="btn btn-success" type="submit">Wish List</button>
                    </span>
                           </form> </div>                  
                   
                          </div>
                <div class="col-xs-12">
         
                    <div style="width:100%;border-top:1px solid silver">
                       <div class="tab">
  <button class="tablinks button1" onclick="openCity(event, 'London')" >Property Information</button>
  <button class="tablinks button1" onclick="openCity(event, 'Paris')">Additional Charges</button>
  <button class="tablinks button1" onclick="openCity(event, 'Tokyo')">Contact Information</button>
</div>

<div id="London" class="tabcontent">
  <div class="row form-group">
               <label class="col-md-2 control-label" for="sms">Property Type</label>
            <div class="col-md-1">
		<div>
			<%=rs.getString("propertytype") %>
            </div>
		</div>
               <label class="col-md-2 control-label" for="village">Available As:</label>
            <div class="col-md-1">
		<div class="input-group">
              <%=rs.getString("available") %>
            </div> </div>
            <label class="col-md-2 control-label" for="sms">Available From:</label>
            <div>
		<div>
			<%=rs.getString("availabledate") %>
            </div>
		</div>
              </div>
            <div class="row form-group">
     <label class="col-md-2 control-label" for="sms">Age of Construction</label>
            <div class="col-md-3">
		<%=rs.getString("age") %>
		</div>
 </div>
                 <div class="row">
            <div class="col-md-2 panel panel-heading">Description</div>
            <div class="col-md-4 panel panel-heading" style="display:none; color:red" id="address_error"></div>
          </div>
            <div>
		<div class="input-group">
			<%=rs.getString("description") %>
            </div>
	</div>
             
     
    
    
</div>

<div id="Paris" class="tabcontent">
  
     <div class="row form-group">
            <label class="col-md-2 control-label" for="">Covered Vehicle Parking</label>
             <div class="col-md-2">
              <%=rs.getString("vehicle") %>
            </div>
              <label class="col-md-2 control-label" for="village">Monthly Maintenance</label>
                <div class="col-md-2">
		<div class="input-group">
			₹<%=rs.getString("monthly") %>
            </div>
          </div>
              <label class="col-md-2 control-label" for="village">Security Deposit</label>
                <div class="col-md-2">
		₹<%=rs.getString("security") %>
          </div>
        
          </div>
               <div class="row form-group">
            <label class="col-md-2 control-label" for="">Water and Electricity Bill Included?</label>
             <div class="col-md-2">
            <%=rs.getString("electricity") %>
            </div>
              </div>
</div>

<div id="Tokyo" class="tabcontent">
          <div class="row form-group">
              
            <label class="col-md-2 control-label" for="first_name">Name</label>  
            <div class="col-md-2">
			<div class="input-group">
			<%=rs.getString("name") %> (<%=rs.getString("relate") %>)
            </div></div>
            <label class="col-md-2 control-label" for="ccmobile">Contact No.</label>
            <div class="col-md-3">
    <div class="input-group">
			<%=rs.getLong("mobile") %>
		
            </div>
	</div>
                        <%
                            if(session.getAttribute("username")!=null)
                            {%>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Message to Landlord</button>
<%
    } else {out.print("login to contact Landlord");}%>

<div class="modal fade" id="exampleModal"  role="dialog" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
        
      <div class="modal-body">
          <form action="contactprocess.jsp" method="post">
          	<div class="form-group">
                    <input type="hidden" class="form-control" id="name" name="name" placeholder="Name" value="<%= session.getAttribute("uname") %>" required>
					</div>
					<div class="form-group">
						<input type="hidden" class="form-control" id="email" name="uemail" value="<%= session.getAttribute("username") %>" placeholder="Email" required>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number" required>
					</div>
              <input type="hidden" name="email" value="<%= rs.getString("email") %>">
              <input type="hidden" name="title" value="<%=rs.getString("title") %>">
                     <input type="hidden" name="postid" value="<%= postid %>" >
                    <div class="form-group">
                    <textarea class="form-control" type="textarea" id="message" placeholder="Message" name="message" maxlength="140" rows="7"></textarea>
                        </div>
              
                    <input type="submit" value="Submit" class="btn btn-primary">
                 
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Exit</button>
        
      </div>
    </div>
  </div>
</div>
          

          </div>
          </div>
                         <div class="row">
            <div class=" panel panel-heading">Contact Address</div>
            <div class="col-md-4 panel panel-heading" style="display:none; color:red" id="address_error"></div>
          </div>

        
          <div class="row form-group">
            <label class="col-md-3 control-label" for="street_address">Address Line 1</label>
            <div class="col-md-5">
		<div class="input-group">
			<%=rs.getString("address") %>
            </div>
            <div class="input-group">
			<%=rs.getString("state") %>
            </div>
	</div>
          </div>
         
</div>   
                       
                    </div>
             </div>  
        </div>
        </div>
        </div>
            <div id="carousel-example-generic" class="carousel slide container" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>
            <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
         <center> 
      <img src="<%=request.getContextPath()%>/images/<%= rs.getString("image1")%>" class="img-responsive" alt="hello" style="width: 850px; height: 500px">
            </center>  
    </div>
      <div class="item">
          <center>
      <img src="<%=request.getContextPath()%>/images/<%= rs.getString("image2")%>" class="img-responsive" alt="hello" style="width: 850px; height: 500px" >
          </center>
      </div>
    <div class="item">
        <center>
      <img src="<%=request.getContextPath()%>/images/<%= rs.getString("image3")%>" class="img-responsive" alt="hello" style="width: 850px; height: 500px" >
        </center>
    </div>
   
        </div>
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
        
                </div>
       
        
      <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #floating-panel {
        position: absolute;
        top: 10px;
        left: 25%;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
        text-align: center;
        font-family: 'Roboto','sans-serif';
        line-height: 30px;
        padding-left: 10px;
      }
    </style>
  </head>
  <div style="width: 100%; height: 400px">
      <% 
      String location=rs.getString("city");
      %>
    <div>
        <input id="address" type="textbox" value="<%=rs.getString("address") %>" hidden>
      <input id="submit" type="button" value="Click to get location">
    </div>
    <div id="map"></div>
    <script>
      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 8,
          center: {lat: 30.18329833, lng: 78.69409652}
        });
        var geocoder = new google.maps.Geocoder();

        document.getElementById('submit').addEventListener('click', function() {
          geocodeAddress(geocoder, map);
        });
      }

      function geocodeAddress(geocoder, resultsMap) {
        var address = document.getElementById('address').value;
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
              map: resultsMap,
              position: results[0].geometry.location
            });
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBY2PhyENrPEA1WQjhugEo5ZtFIfuIfKH8&callback=initMap">
    </script>
  </div>
       
 <% } 
}catch(Exception e){
out.print(e);}
        
        %>
       
        <%@include file="footer.jsp" %>