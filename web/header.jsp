<%-- 
    Document   : header
    Created on : Jan 25, 2018, 2:39:32 PM
    Author     : Himanshu Pant
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<title>A new Way to Find Rooms</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- //for-mobile-apps -->
<link href="files/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
<!-- pignose css -->
<link href="files/pignose.layerslider.css" rel="stylesheet" type="text/css" media="all" />


<!-- //pignose css -->
<link href="files/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script type="text/javascript" src="files/bootstrap/js/jquery.min.js"></script>
<!-- //js -->
<!-- for bootstrap working -->
<script type="text/javascript" src="files/bootstrap/js/bootstrap.min.js"></script>
<!-- //for bootstrap working -->
<script src="js/jquery.easing.min.js"></script>
</head>
<body>
    <div class="header-bot">
	<div class="container">
		<div class="col-md-3">
			<h1><a href="index.jsp">RoomRent</a></h1>
		</div>
             <div class="col-sm-5 col-md-5">
                 <form class="navbar-form" role="search" style="width:auto;" action="search.jsp">
           
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search" name="search" size="40" required oninvalid="this.setCustomValidity('There must be some keyword')">
            <span class="input-group-btn">
                <button class="btn btn-success" type="submit">Go!</button>
                    </span>
        </div>
           
        </form>
    </div>
		
		<div class="col-md-3 header-right footer-bottom">
			<ul>
				<li><a class="fb" href="https://www.facebook.com/RoomRent-601421940224991/"></a></li>
				<li><a class="twi" href="https://twitter.com/RoomRent5"></a></li>
				<li><a class="insta" href="#"></a></li>
				<li><a class="you" href="#"></a></li>
                
			</ul>
		</div>
            
        
		<div style="text-align: right">Customer care</div>
	</div>
</div>
<!-- //header-bot -->
<!-- banner -->
<div class="ban-top">
	<div class="container">
		<div class="top_nav_left">
			<nav class="navbar navbar-default">
			  <div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        
				  </button>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse menu--shylock" id="bs-example-navbar-collapse-1">
				  <ul class="nav navbar-nav menu__list">
					<li class="active menu__item menu__item--current"><a class="menu__link" href="index.jsp">RoomRent<span class="sr-only">(current)</span></a></li>
					<li class="dropdown menu__item">
						<a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Type <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<div>
									
										
								
									
										<ul class="multi-column-dropdown">
											<li><a href="rent.jsp">Rent</a></li>
											<li><a href="pg.jsp">PG</a></li>
										</ul>
									
									
								</div>
							</ul>
					</li>
					<li class="dropdown menu__item">
						<a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Sell<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<div>
									<div>
										<ul  class="multi-column-dropdown">
											<li><a href="post.jsp">Post Ad</a></li>
											
										</ul>
									</div>
								</div>
                        </ul>
					</li>
                                        
                                        <%
    if ((session.getAttribute("username") == null) || (session.getAttribute("username") == "")) {
%>
<li class=" menu__item"><a class="menu__link" href="login.jsp">Login</a></li>
<%} else {
%>
<li class="dropdown menu__item">
						<a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <%=session.getAttribute("uname") %> <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<div>
									
										
								
									
										<ul class="multi-column-dropdown">
											<li><a href="wishlist.jsp">Wish List</a></li>
                                                                                        <%if(session.getAttribute("uaadhar")==null){ %>
                                                                                        <li><a href="aadhar_tenant.jsp">Verify Account</a></li>
                                                                                        <% }%>
                                                                                        <li><a href="modifypassword.jsp">Change Password</a></li>
											<li><a href="logout.jsp">Logout</a></li>
										</ul>
									
									
								</div>
							</ul>
					</li>
<%
    }
%>
				  </ul>
				</div>
			  </div>
			</nav>	
		</div>
                                  <div class="top_nav_right">
                    
                                      <div class="cart box_1" style="background-color:#00bcd4">
						<a href="post.jsp">
						</a>
                                          <b><p style="color: #ffffff"><a href="post.jsp">Post Ad(Free)</a></p><b>
						
			</div>	
		</div>
	</div>
</div>

</body>

</html>
