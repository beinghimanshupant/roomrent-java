<%-- 
    Document   : managepost
    Created on : Feb 24, 2018, 12:13:31 PM
    Author     : Himanshu Pant
--%>

<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="aheader.jsp"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="../files/main.css">
<script type="text/javascript" src="../files/view.js"></script>
<div class="container">
    <div class="row">
        <ul class="thumbnails">
            <div style="width:50%; alignment-adjust: auto;">
     
          
  
            </div>
        
            
            
<% String s1 = "invalid";
    String driver = (String)application.getInitParameter("driver");
            String username=(String)application.getInitParameter("username");
            String url=(String)application.getInitParameter("url");
            String password=(String)application.getInitParameter("password");
    String sql=null;
   
    //out.print(sort);
    Statement st;
   
     sql="select postid,title,email,price,city,postdate,image1,bedroom,availabledate,propertyfor,verify from postadd where verify=0 order by postdate desc " ;
   
    try{  
Class.forName(driver);  
 Connection con=DriverManager.getConnection(url,username,password);
  st = con.createStatement();
    ResultSet rs=st.executeQuery(sql);
    boolean status = rs.next();
    if(!status){
    out.print("<h2> No New posts are available </h2>");
    }
    boolean s = rs.previous();
    while(rs.next()){
       int verify = rs.getInt("verify");
         String title=rs.getString("title");
          String price=String.valueOf(rs.getInt("price"));
          
            PreparedStatement p = con.prepareStatement("select validate from seller where email=?");
 p.setString(1, rs.getString("email"));
 ResultSet r =p.executeQuery();
if(r.next()){
s1="valid";
}
            
    
      %>  
      <div>
      <div class="col-md-4">
           <div class="thumbnail">
               <% if(s1.equals("valid")){out.print("&#10004;");} else{out.print("&#10008;");}%>    

          <img src="<%=request.getContextPath()%>/images/<%= rs.getString("image1")%>" class="img-responsive" alt="hello" style="width:300px;height:200px">
 <div class="caption">
     <div style="height: 250px">
     <h4> <%=  title %>, <%= rs.getInt("bedroom")%>BHK</h4>
     <h5>Location :<%= rs.getString("city")%></h5>
     <h6> Price Rs<%=  price %> </h6>     
     <h6>Posted On<%=  rs.getDate("postdate") %></h6>
     <h6>Available as <%= rs.getString("propertyfor")%> from <%= rs.getString("availabledate")%></h6>
                        
                        <p align="center"><a href="aeditpost.jsp?postid=<%= rs.getInt("postid")%>" class="btn btn-primary btn-block">Edit</a>
                        </p>
                    </div>
 </div>  </div></div> </div>
 <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        </div>
        <div class="modal-body">
          <img src="" class="enlargeImageModalSource" style="width: 100%;">
        </div>
      </div>
    </div>
</div>
            <% }
}catch(Exception e){
out.print(e);}
    %>
        
        </ul>
    </div>
</div>