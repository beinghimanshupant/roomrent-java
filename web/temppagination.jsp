<%-- 
    Document   : temppagination
    Created on : Mar 13, 2018, 7:11:51 PM
    Author     : Himanshu Pant
--%>

<%@page import="java.sql.*"%>
<%!
public int nullIntconv(String str)
{
int conv=0;
if(str==null)
{
str="0";
}
else if((str.trim()).equals("null"))
{
str="0";
}
else if(str.equals(""))
{
str="0";
}
try{
conv=Integer.parseInt(str);
}
catch(Exception e)
{
System.out.print("error"+e);
}
return conv;
}
%>
<%
    String driver = (String)application.getInitParameter("driver");
            String username=(String)application.getInitParameter("username");
            String url=(String)application.getInitParameter("url");
            String password=(String)application.getInitParameter("password");

Connection conn = null;
Class.forName(driver).newInstance();
conn = DriverManager.getConnection(url,username,password);

ResultSet rsPagination = null;
ResultSet rsRowCnt = null;

PreparedStatement psPagination=null;
PreparedStatement psRowCnt=null;

int iShowRows=5; // Number of records show on per page
int iTotalSearchRecords=10; // Number of pages index shown

int iTotalRows=nullIntconv(request.getParameter("iTotalRows"));
int iTotalPages=nullIntconv(request.getParameter("iTotalPages"));
int iPageNo=nullIntconv(request.getParameter("iPageNo"));
int cPageNo=nullIntconv(request.getParameter(""));

int iStartResultNo=0;
int iEndResultNo=0;

if(iPageNo==0)
{
iPageNo=0;
}
else{
iPageNo=((iPageNo-1)*iShowRows);
}
String sqlPagination="SELECT SQL_CALC_FOUND_ROWS * FROM  postcontact limit "+iPageNo+","+iShowRows+"";
//String sqlPagination="SELECT SQL_CALC_FOUND_ROWS * FROM  postcontact limit 15,5";
psPagination=conn.prepareStatement(sqlPagination);
rsPagination=psPagination.executeQuery();

//// this will count total number of rows
String sqlRowCnt="SELECT FOUND_ROWS() as cnt";
psRowCnt=conn.prepareStatement(sqlRowCnt);
rsRowCnt=psRowCnt.executeQuery();

if(rsRowCnt.next())
{
iTotalRows=rsRowCnt.getInt("cnt");
}
%>
<html>
<head>
<title>Pagination of JSP page</title>

</head>
<body>
<form name=?frm?>
<input type="hidden" name="iPageNo" value="<%=iPageNo%>">
<input type="hidden" name="cPageNo" value=?<%=cPageNo%>?>
<input type="hidden" name="iShowRows" value=?<%=iShowRows%>?>
<table width=?100%? cellpadding=?0? cellspacing=?0? border=?1? >
<tr>
<td>Id</td>
<td>Name</td>
<td>Value</td>
</tr>
<%
while(rsPagination.next())
{
%>
<tr>
<td><%=rsPagination.getString("postid")%></td>
<td><%=rsPagination.getString("name")%></td>
<td><%=rsPagination.getString("email")%></td>
</tr>
<%
}
%>
<%
//// calculate next record start record and end record
try{
if(iTotalRows<(iPageNo+iShowRows))
{
iEndResultNo=iTotalRows;
}
else
{
iEndResultNo=(iPageNo+iShowRows);
}

iStartResultNo=(iPageNo+1);
iTotalPages=((int)(Math.ceil((double)iTotalRows/iShowRows)));

}
catch(Exception e)
{
out.print(e);
}

%>
<tr>
<td colspan=?3?>

<div>
<%
        //// index of pages 
         
        int i=0;
        int cPage=0;
        if(iTotalRows!=0)
        {
        cPage=((int)(Math.ceil((double)iEndResultNo/(iTotalSearchRecords*iShowRows))));
         
        int prePageNo=(cPage*iTotalSearchRecords)-((iTotalSearchRecords-1)+iTotalSearchRecords);
       
         
        for(i=((cPage*iTotalSearchRecords)-(iTotalSearchRecords-1));i<=(cPage*iTotalSearchRecords);i++)
        {
          if(i==((iPageNo/iShowRows)+1))
          {
          %>
          <a href="temppagination.jsp?iPageNo=<%=i%>" style="cursor:pointer;color: red"><b><%=i%></b></a>
          <%
          }
          else if(i<=iTotalPages)
          {
          %>
          <a href="temppagination.jsp?iPageNo=<%=i%>"><%=i%></a>
          <% 
          }
        }
        }
      %>
<b>Rows <%=iStartResultNo%> - <%=iEndResultNo%>   Total Result  <%=iTotalRows%> </b>
</div>
</td>
</tr>
</table>
</form>
</body>
</html>
<%
    try{
         if(psPagination!=null){
             psPagination.close();
         }
         if(rsPagination!=null){
             rsPagination.close();
         }
          
         if(psRowCnt!=null){
             psRowCnt.close();
         }
         if(rsRowCnt!=null){
             rsRowCnt.close();
         }
          
         if(conn!=null){
          conn.close();
         }
    }
    catch(Exception e)
    {
        out.print(e);
    }
%>