<%-- 
    Document   : addwishlist
    Created on : Mar 29, 2018, 12:23:11 PM
    Author     : Himanshu Pant
--%>

<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
String islogin=(String)session.getAttribute("islogin");
if(islogin==null){
request.setAttribute("notlogin_msg","Sorry,Please do Login first");

%>
<jsp:forward page="checklogin.jsp"></jsp:forward>
<%
}
%>
<% 
    
String email =  (session.getAttribute("username")).toString();
String postid = request.getParameter("postid");
String title = request.getParameter("title");
String image = request.getParameter("image");
String bedroom = request.getParameter("bedroom");
String price = request.getParameter("price");
String location = request.getParameter("location");
 String driver = (String)application.getInitParameter("driver");
            String username=(String)application.getInitParameter("username");
            String url=(String)application.getInitParameter("url");
            String password =(String)application.getInitParameter("password");
            try{  
Class.forName(driver);  
 Connection con=DriverManager.getConnection(url,username,password);
 PreparedStatement ps=con.prepareStatement("insert into wishlist values(?,?,?,?,?,?,?)");
            //ps.setString(1, "");
             ps.setString(1,postid);
             ps.setString(2, email);
             ps.setString(3, title); 
             ps.setString(4, image);
             ps.setString(5,bedroom);
             ps.setString(6, price);
             ps.setString(7, location);
              int i=ps.executeUpdate();
             if(i!=0)
             { %>
             Added to Wishlist
             <jsp:forward page="display.jsp"></jsp:forward>
             <%
            }
}catch(Exception e){
            out.print(e);
            }

%>
