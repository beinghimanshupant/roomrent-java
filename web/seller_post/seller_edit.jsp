<%-- 
    Document   : seller_edit
    Created on : Feb 27, 2018, 3:53:00 PM
    Author     : Himanshu Pant
--%>

<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- //for-mobile-apps -->
<link href="../files/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
<!-- pignose css -->
<link href="files/pignose.layerslider.css" rel="stylesheet" type="text/css" media="all" />


<!-- //pignose css -->
<link href="files/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script type="text/javascript" src="../files/bootstrap/js/jquery.min.js"></script>
<!-- //js -->
<!-- for bootstrap working -->
<script type="text/javascript" src="../files/bootstrap/js/bootstrap.min.js"></script>
<!-- //for bootstrap working -->
<script src="js/jquery.easing.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>RoomRent Seller</title>
    </head>
    <body>
	<div class="container">
		<div class="col-md-3">
			<h1><a href="#">RoomRent</a></h1>
            
        
		
	</div>
            <div style="text-align: right">Seller Panel</div>
</div>
<!-- //header-bot -->
<!-- banner -->

<div class="ban-top">
		<div class="top_nav_left">
			<nav class="navbar navbar-default">
			  <div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					
                                        
				  </button>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse menu--shylock" id="bs-example-navbar-collapse-1">
				  <ul class="nav navbar-nav menu__list">
					<li class="active menu__item menu__item--current"><a class="menu__link" href="#">RoomRent<span class="sr-only">(current)</span></a></li>
					<li class="menu__item">
                                           	
					</li>
					<li class="menu__item">
                                            <a href="../post.jsp" role="button" aria-haspopup="true" aria-expanded="false">Post Another Add <span class="sr-only"></span></a>
							
					</li>
                                        
                                       		  </ul>
				</div>
			  </div>
			</nav>	
		</div>
		
	</div>
    </body>
</html>
<div class="container">
    <div class="row">
        <ul class="thumbnails">
            <div style="width:50%; alignment-adjust: auto;">
     
          
  
            </div>
        
<% 
int id=Integer.parseInt(request.getParameter("postid"));

String driver = (String)application.getInitParameter("driver");
            String username=(String)application.getInitParameter("username");
            String url=(String)application.getInitParameter("url");
            String password=(String)application.getInitParameter("password");
    String sql=null;
   
    //out.print(sort);
    
   
     sql="select title,price,mobile,description,email from postadd where postid = ? " ;
   
    try{  
Class.forName(driver);  
 Connection con=DriverManager.getConnection(url,username,password);
  PreparedStatement ps=con.prepareStatement(sql);
  ps.setInt(1, id);
    ResultSet rs=ps.executeQuery();
    while(rs.next()){
      %>  
      <form method="post" action="seller_edit_process.jsp">
          <input type="hidden" name="postid" value="<%=id %>">
          Title : <input type="text" name="title" value="<%=rs.getString("title") %>" >
          Price : <input type="number" value="<%= rs.getInt("price") %>" name="price" >
          Contact number : <input type="number" value="<%= rs.getLong("mobile") %>" name="mobile" >
          Description : <textarea name="description" value="<%=rs.getString("description") %>"><%=rs.getString("description") %></textarea>
          <input type="submit" value="Change">
      </form>
 <form action="postdelete.jsp?postid=<%=id %>" method="post" class="col-md-3">
    <input type="hidden" name="event" value="delete">
    <input type="submit" value="Delete"  class="btn btn-danger">
</form>
            <% }
}catch(Exception e){out.print("no active Advertisement post new"+e);}
    %>
        
        </ul>
    </div>
</div>