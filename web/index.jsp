<%-- 
    Document   : index
    Created on : Jan 19, 2018, 9:25:11 PM
    Author     : Himanshu Pant
--%>

<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<meta name="viewport" content="width=device-width, initial-scale=1">
<%@ include file="header.jsp" %>
<!DOCTYPE html>

        
   

    <!-- Page Content -->
    <div class="container">

      <div class="row">

        <div class="col-lg-3">

        </div>
        <!-- /.col-lg-3 -->
   

         <div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
    
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
      <div class="item active">
          <img src="images/Untitled.jpg" style="width: 1300px; height: 400px" >
      <div class="carousel-caption hidden-xs">
        <h3> Welcome to Roomrent </h3>
        
   
      </div>
    </div>
        <%
        String driver = (String)application.getInitParameter("driver");
            String username=(String)application.getInitParameter("username");
            String url=(String)application.getInitParameter("url");
            String password =(String)application.getInitParameter("password");
             try{  
//Class.forName("com.mysql.jdbc.Driver");  
//Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/RoomRent","root","");
 Class.forName(driver);  
 Connection con=DriverManager.getConnection(url,username,password);
 PreparedStatement ps = con.prepareStatement("select * from homepage ORDER BY id DESC LIMIT 3");
ResultSet rs=ps.executeQuery();
 while(rs.next()){ 
            
             
             
        %>
        
    <div class="item">
        <center>
        <a href="product.jsp?postid=<%= rs.getInt("postid")%>">
            <img src="<%=request.getContextPath()%>/images/<%= rs.getString("banner")%>" style="width: 1300px; height: 400px"  class="img-responsive">
            </center>
            <div class="carousel-caption hidden-xs">
                <h3> <%= rs.getString("title")%> </h3> </a>
        
        
      </div>
    </div>
        <% } %>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
  <br>
                        <div>
                        <h3>New Arrivals-Rent</h3> <a href="rent.jsp" >more..</a>
                        </div>

          <div class="row">
              <%
PreparedStatement ps1 = con.prepareStatement("select * from postadd where propertyfor='Rent'and verify=1 order by postid desc limit 6");
ResultSet rs1=ps1.executeQuery();
 while(rs1.next()){ 
                  %>
            <div class="col-md-4">
              <div class="card h-100">
                <a href="product.jsp?postid=<%= rs1.getInt("postid")%>"><img class="card-img-top" src="<%=request.getContextPath()%>/images/<%= rs1.getString("image1")%>" alt="" style="width:300px;height:200px"></a>
                <div class="card-body">
                    <div style="height: 80px">
                  <h4 class="card-title">
                    <%= rs1.getString("title")%>
                  </h4>
                  <h5>  Price: Rs<%= rs1.getInt("price")%></h5>
                  <p class="card-text"> <%= rs1.getString("city")%>,India</p>
                    </div></div>
              </div>
            </div>
                  <% }%>
          </div>

            <br>
                        <div>
                        <h3>New Arrivals-PG</h3> <a href="pg.jsp" >more..</a>
                        </div>

          <div class="row">
              <%
PreparedStatement ps2 = con.prepareStatement("select * from postadd where propertyfor='PG' and verify=1 order by postid desc limit 6");
ResultSet rs2=ps2.executeQuery();
 while(rs2.next()){ 
                  %>
            <div class="col-md-4">
              <div class="card h-100">
                <a href="product.jsp?postid=<%= rs2.getInt("postid")%>"><img class="card-img-top" src="<%=request.getContextPath()%>/images/<%= rs2.getString("image1")%>" alt="" style="width:300px;height:200px"></a>
                <div class="card-body">
                    <div style="height: 80px">
                  <h4 class="card-title">
                    <%= rs2.getString("title")%>
                  </h4>
                  <h5>  Price: Rs<%= rs2.getInt("price")%></h5>
                  <p class="card-text"> <%= rs2.getString("city")%>,India</p>
                    </div></div>
              </div>
            </div>
            <% }%>
          </div>
            
  
          <!-- /.row -->

       
        <!-- /.col-lg-9 -->

      </div>
      <!-- /.row -->

    </div>
        <%
        }
        catch(Exception e)
{}
        %>
    <!-- /.container -->

<%@include file="footer.jsp" %>