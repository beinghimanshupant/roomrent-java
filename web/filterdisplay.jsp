<%-- 
    Document   : filterdisplay
    Created on : Jan 26, 2018, 10:31:47 PM
    Author     : Himanshu Pant
--%>
<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<link rel="stylesheet" type="text/css" href="files/main.css">
<div class="container">
    <div class="row">
        <ul class="thumbnails">
            <div style="width:300px;">
     
           <form method="post" action="#" name="Form">
<label for="sortby" >Sort By</label>
<select name="sort" onchange="document.Form.submit()()">
             
            <option value="Popular">Popular</option>
            <option value="postdate ASC">Posted On(Recent First)</option>
            <option value="postdate desc">Posted On(Oldest First)</option>
            <option value="price desc">Price(High to Low)</option>
            <option value="price asc">Price(Low to High)</option>
        </select>
    </form>
  
            </div>
        
            
            
<%
    String driver = (String)application.getInitParameter("driver");
            String username=(String)application.getInitParameter("username");
            String url=(String)application.getInitParameter("url");
            String password=(String)application.getInitParameter("password");
    String location=request.getParameter("location");
                    int min=Integer.parseInt(request.getParameter("min"));
                            int max=Integer.parseInt(request.getParameter("max"));
                            String s=null;
                      
  String sort=request.getParameter("sort");
  String sql=null;
    //out.print(sort);
    PreparedStatement st;
    if(location!=null)
    sql="select postid,title,price,city,state,postdate,image1 from postadd where city=? and price between "+min+" and "+max+ " order by "+sort;
      try{  
Class.forName(driver);  
 Connection con=DriverManager.getConnection(url,username,password);
  st = con.prepareStatement(sql);
  st.setString(1,location);
  //st.setInt(2,min);
  //st.setInt(3,max);
    ResultSet rs=st.executeQuery();
    boolean r = rs.next();
    if(!r){out.print("No match found!");}
    boolean b = rs.previous();
    while(rs.next()){
         String title=rs.getString("title");
          String price=String.valueOf(rs.getInt("price"));
      %>  
      <div>
      <div class="col-md-4">
           <div class="thumbnail">
          <img src="<%=request.getContextPath()%>/images/<%= rs.getString("image1")%>" class="img-responsive" alt="hello" style="width:300px;height:200px">
 <div class="caption">
     <h4> <%=  title %></h4>
     <h5>Location :<%= rs.getString("city")%></h5>
     <h6> Price Rs<%=  price %> </h6>     
     <h6>Posted On<%=  rs.getDate("postdate") %></h6>
                        <p align="center"><a href="product.jsp?postid= <%= rs.getString("postid")%>" class="btn btn-primary btn-block">Buy</a>
                        </p>
                    </div>
           </div>  </div></div> <% }
}catch(Exception e){out.print(e);}
    %>
        
        </ul>
    </div>
</div>
    <%@include file="footer.jsp" %>