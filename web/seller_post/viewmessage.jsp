<%-- 
    Document   : viewmessage
    Created on : Mar 28, 2018, 11:10:23 AM
    Author     : Himanshu Pant
--%>

<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"/>
<%
    int id = Integer.parseInt(request.getParameter("id"));
    String driver = (String)application.getInitParameter("driver");
            String username=(String)application.getInitParameter("username");
            String url=(String)application.getInitParameter("url");
            String password=(String)application.getInitParameter("password");
             try{  
Class.forName(driver);  
 Connection con=DriverManager.getConnection(url,username,password);
  PreparedStatement ps = con.prepareStatement("select * from contactseller where id=?");
    ps.setInt(1,id);
    ResultSet rs=ps.executeQuery();
 while(rs.next()){ %>
 <div class="container">    
                <div class="jumbotron">
                  <div class="row">
                     
                      <div class="col-md-8 col-xs-12 col-sm-6 col-lg-8">
                          <div class="container" style="border-bottom:1px solid black">
                              <h2><%= rs.getString("name") %></h2>
                          </div>
                            <hr>
                          <ul class="container details">
                              <li><p><span class="glyphicon glyphicon-earphone one" style="width:50px;"></span>+91 <%=rs.getString("mobile") %> </p></li>
                            <li><p><span class="glyphicon glyphicon-envelope one" style="width:50px;"></span><%=rs.getString("uemail") %></p></li>
                            <li><p> The above customer messaged you regarding your Advertisement <B> <%= rs.getString("title") %>.</B>
                                <p> Says or wants to know that <%= rs.getString("message") %> .
                                <p> Kindly contact or call back him ASAP.
                          </ul>
                      </div>
                  </div>
                </div>
 <%
     
 
 }
 
             } catch(Exception e){
             out.print(e);
             }
    %>
