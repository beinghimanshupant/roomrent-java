<%-- 
    Document   : temppanel
    Created on : May 1, 2018, 5:11:04 PM
    Author     : Himanshu Pant
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<%
if(session.getAttribute("selleremail")==null)
response.sendRedirect("seller_login.jsp");

%>
<!------ Include the above in your HEAD tag ---------->
<link href="../files/sellerpanel.css" rel="stylesheet" type="text/css"/>
<div class="container">
	<div class="row">
           	<div class="col-md-4">
                     <a href="sellerpost.jsp">
			<div class="dash-box dash-box-color-1">
				<div class="dash-box-icon">
					<i class="glyphicon"></i>
				</div>
				<div class="dash-box-body">
					<span class="dash-box-count"></span>
                                        <span class="dash-box-title"> <h2>View Advertisements </h2> </span>
				</div>
				
				<div class="dash-box-action">
		
				</div>				
			</div>
                     </a>
		</div>
		<div class="col-md-4">
                    <a href="message.jsp">
			<div class="dash-box dash-box-color-2">
				<div class="dash-box-icon">
					<i class="glyphicon mail-dropdown"></i>
				</div>
				<div class="dash-box-body">
				
                                    <span class="dash-box-title"><h2>View Messages </h2></span>
				</div>
				
							
			</div>
                    </a>
		</div>
		<div class="col-md-4">
                    <a href="../post.jsp">
			<div class="dash-box dash-box-color-3">
				<div class="dash-box-icon">
					<i class="glyphicon mail-box"></i>
				</div>
				<div class="dash-box-body">
				
                                        <span class="dash-box-title"><h2>Post Advertisements </h2></span>
				</div>				
			</div>
                    </a>
		</div>
            <div class="col-md-4">
                     <a href="seller_aadhar.jsp">
			<div class="dash-box dash-box-color-1">
				<div class="dash-box-icon">
					<i class="glyphicon"></i>
				</div>
				<div class="dash-box-body">
					<span class="dash-box-count"></span>
                                        <span class="dash-box-title"> <h2>Verify Aadhar </h2> </span>
				</div>
				
				<div class="dash-box-action">
		
				</div>				
			</div>
                     </a>
		</div>
            <div class="col-md-4">
                    <a href="samodifypassword.jsp">
			<div class="dash-box dash-box-color-2">
				<div class="dash-box-icon">
					<i class="glyphicon mail-dropdown"></i>
				</div>
				<div class="dash-box-body">
				
                                    <span class="dash-box-title"><h2>Change Password </h2></span>
				</div>
				
							
			</div>
                    </a>
		</div>
	</div>
    <a href="../index.jsp" >RoomRent </a>
</div>
