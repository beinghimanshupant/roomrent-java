<%-- 
    Document   : forgotpassword
    Created on : May 4, 2018, 4:21:09 PM
    Author     : Himanshu Pant
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<% 
String message="";
String e = request.getParameter("msg");
if(e!=null){
out.print("<font color='red'><div style='text-align: center'>Sorry this email address is not registered. Click here for <a href='signup.jsp'>Sign Up</a> </div>");
}
%>

<div class="container">
	<div class="row">
	
            <div class="col-md-6 col-md-offset-3">
                
                <form accept-charset="UTF-8" role="form" id="login-recordar" method="post" action="processforgotpassword.jsp">
      <fieldset>
        <span class="help-block">
          Email address you use to log in to your account
          <br>
          We'll send you an email with instructions to choose a new password.
        </span>
        <div class="form-group input-group">
          <span class="input-group-addon">
            @
          </span>
          <input class="form-control" placeholder="Email" name="email" type="email" required="">
        </div>
        <button type="submit" class="btn btn-primary btn-block" id="btn-olvidado">
          Continue
        </button>
      </fieldset>
    </form>
  </div>
            </div>
        </div>

