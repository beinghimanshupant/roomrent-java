
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.sql.*;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Himanshu Pant
 */
public class Listener implements ServletContextListener {
    
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext context=sce.getServletContext();
        String driver = context.getInitParameter("driver");
            String username=context.getInitParameter("username");
            String url=context.getInitParameter("url");
            String password=context.getInitParameter("password");
            try{  
Class.forName(driver);  
 Connection con=DriverManager.getConnection(url,username,password);
 PreparedStatement ps=con.prepareStatement("CREATE TABLE IF NOT EXISTS `admin` (\n" +
"  `id` varchar(10) NOT NULL,\n" +
"  `name` varchar(50) NOT NULL,\n" +
"  `email` varchar(100) NOT NULL,\n" +
"  `password` varchar(50) NOT NULL,\n" +
"  `refered` varchar(50) NOT NULL DEFAULT 'default',\n" +
"  PRIMARY KEY (`id`)\n" +
") ");
		ps.executeUpdate();
        PreparedStatement ps1=con.prepareStatement("CREATE TABLE IF NOT EXISTS postadd (\n" +
"  postid int(50) NOT NULL AUTO_INCREMENT,\n" +
"  mobile bigint(10) NOT NULL,\n" +
"  relate varchar(20) NOT NULL,\n" +
"  `name` varchar(50) NOT NULL,\n" +
"  email varchar(50) NOT NULL,\n" +
"  propertyfor varchar(20) NOT NULL,\n" +
"  propertytype varchar(30) NOT NULL,\n" +
"  city varchar(20) NOT NULL,\n" +
"  country varchar(20) NOT NULL DEFAULT 'INDIA',\n" +
"  state varchar(50) NOT NULL,\n" +
"  address varchar(100) NOT NULL,\n" +
"  available varchar(15) NOT NULL,\n" +
"  availabledate varchar(15) NOT NULL,\n" +
"  age varchar(20) NOT NULL,\n" +
"  title varchar(50) NOT NULL,\n" +
"  price int(10) NOT NULL,\n" +
"  carpetarea varchar(10) NOT NULL,\n" +
"  bedroom int(10) NOT NULL,\n" +
"  kitchen varchar(5) NOT NULL,\n" +
"  bathroom int(10) NOT NULL,\n" +
"  vehicle varchar(5) NOT NULL,\n" +
"  monthly varchar(10) NOT NULL,\n" +
"  `security` varchar(10) NOT NULL,\n" +
"  electricity varchar(10) NOT NULL,\n" +
"  image1 varchar(100) NOT NULL,\n" +
"  image2 varchar(50) DEFAULT NULL,\n" +
"  image3 varchar(25) DEFAULT NULL,\n" +
"  description varchar(1000) NOT NULL,\n" +
"  postdate date NOT NULL,\n" +
"  priority int(11) NOT NULL DEFAULT '0',\n" +
"  verify int(10) NOT NULL DEFAULT '0',\n" +
"  PRIMARY KEY (postid)\n" +
")");
		ps1.executeUpdate();  
                 PreparedStatement ps2=con.prepareStatement("CREATE TABLE IF NOT EXISTS `seller` (\n" +
"  `name` varchar(50) NOT NULL,\n" +
"  `email` varchar(50) NOT NULL,\n" +
"  `password` varchar(20) NOT NULL,\n" +
"  `validate` varchar(10) NOT NULL DEFAULT '0',\n" +
"  `aadhar` varchar(15) DEFAULT NULL,\n" +
"  `status` varchar(15) NOT NULL DEFAULT 'not',\n" +
"  PRIMARY KEY (`email`)\n" +
") ");
		ps2.executeUpdate(); 
                 PreparedStatement ps3=con.prepareStatement("CREATE TABLE IF NOT EXISTS `signup` (\n" +
"  `name` varchar(25) DEFAULT 'User',\n" +
"  `email` varchar(50) NOT NULL,\n" +
"  `mobile` bigint(10) NOT NULL DEFAULT '0',\n" +
"  `password` varchar(20) DEFAULT 'sociallogin',\n" +
"  `aadhar` varchar(20) NOT NULL DEFAULT 'not',\n" +
"  `status` varchar(20) NOT NULL DEFAULT 'not',\n" +
"  PRIMARY KEY (`email`)\n" +
") ");
		ps3.executeUpdate(); 
                 PreparedStatement ps4=con.prepareStatement("CREATE TABLE IF NOT EXISTS `contactseller` (`id` int(10) NOT NULL AUTO_INCREMENT,"+
 " `uemail` varchar(100) NOT NULL,\n"+
 " `semail` varchar(100) NOT NULL,\n"+
  "`name` varchar(50) NOT NULL,\n"+
  "`mobile` varchar(20) NOT NULL,\n"+
  "`message` varchar(200) NOT NULL,\n"+
  "`title` varchar(100) NOT NULL,\n"+
  "`mdate` varchar(50) NOT NULL,\n"+
  "PRIMARY KEY (`id`)\n"+
") ");
		ps4.executeUpdate(); 
                 PreparedStatement ps5=con.prepareStatement("CREATE TABLE IF NOT EXISTS `homepage` (`id` int(11) NOT NULL AUTO_INCREMENT,"+
 " `postid` varchar(10) NOT NULL,\n"+
  "`banner` varchar(50) NOT NULL,\n"+
  "`title` varchar(100) NOT NULL,\n"+
  "PRIMARY KEY (`id`)\n"+
") ");
	ps5.executeUpdate(); 
                PreparedStatement ps6=con.prepareStatement("CREATE TABLE IF NOT EXISTS `wishlist` (\n" +
"  `postid` varchar(11) NOT NULL,\n" +
"  `email` varchar(55) NOT NULL,\n" +
"  `title` varchar(200) NOT NULL,\n" +
"  `image` varchar(150) NOT NULL,\n" +
"  `bedroom` varchar(10) NOT NULL,\n" +
"  `price` varchar(20) NOT NULL,\n" +
"  `location` varchar(20) NOT NULL\n" +
")");
		ps6.executeUpdate(); 
                PreparedStatement ps7=con.prepareStatement("CREATE TABLE IF NOT EXISTS `contactus` (\n" +
"  `id` int(10) NOT NULL AUTO_INCREMENT,\n" +
"  `name` varchar(30) NOT NULL,\n" +
"  `mobile` varchar(50) NOT NULL,\n" +
"  `subject` varchar(20) NOT NULL,\n" +
"  `message` varchar(150) NOT NULL,\n" +
"  `date` varchar(50) NOT NULL,\n" +
"  `process` int(5) NOT NULL,\n" +
"  PRIMARY KEY (`id`)\n" +
")");
		ps7.executeUpdate(); 
                // PreparedStatement ps8=con.prepareStatement("");
		//ps8.executeUpdate(); 
            }catch(Exception e ) { System.out.println(e + driver+"error aa rhi hai kuch");}
       }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println(" RoomRent undeloyed"); //To change body of generated methods, choose Tools | Templates.
    }
    
}
