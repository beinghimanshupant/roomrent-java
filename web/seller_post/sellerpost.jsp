<%-- 
    Document   : sellerpanel
    Created on : Feb 27, 2018, 3:09:28 PM
    Author     : Himanshu Pant
--%>

<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"/>
<div class="container">
    <div class="row">
        <ul class="thumbnails">
            <div style="width:50%; alignment-adjust: auto;">
     
          
  
            </div>
        
<% 
String email=(String)session.getAttribute("selleremail");
//String email="bhuwan@gmail.com";
String driver = (String)application.getInitParameter("driver");
            String username=(String)application.getInitParameter("username");
            String url=(String)application.getInitParameter("url");
            String password=(String)application.getInitParameter("password");
    String sql=null;
   
    //out.print(sort);
    
   
     sql="select postid,title,price,city,postdate,image1,bedroom,availabledate,propertyfor,verify from postadd where email = ? order by postdate desc " ;
   
    try{  
Class.forName(driver);  
 Connection con=DriverManager.getConnection(url,username,password);
  PreparedStatement ps=con.prepareStatement(sql);
  ps.setString(1, email);
    ResultSet rs=ps.executeQuery();
    while(rs.next()){
       int verify = rs.getInt("verify");
         String title=rs.getString("title");
          String price=String.valueOf(rs.getInt("price"));
            
            
    
      %>  
      <div>
      <div class="col-md-4">
           <div class="thumbnail">

          <img src="<%=request.getContextPath()%>/images/<%= rs.getString("image1")%>" class="img-responsive" alt="hello" style="width:300px;height:200px">
 <div class="caption">
     <div style="height: 250px">
     <h4> <%=  title %>, <%= rs.getInt("bedroom")%>BHK</h4>
     <h5>Location :<%= rs.getString("city")%></h5>
     <h6> Price Rs<%=  price %> </h6>     
     <h6>Posted On<%=  rs.getDate("postdate") %></h6>
     <h6>Available as <%= rs.getString("propertyfor")%> from <%= rs.getString("availabledate")%></h6>
                        <p align="center"><a href="sellerview.jsp?postid=<%= rs.getInt("postid")%>" class="btn btn-primary btn-block">View</a>
                        </p>
                        <p align="center"><a href="seller_edit.jsp?postid=<%= rs.getInt("postid")%>" class="btn btn-primary btn-block">Edit</a>
                        </p>
                    </div>
 </div>  </div></div> </div>
 
            <% }
}catch(Exception e){out.print("no active Advertisement post new");}
    %>
        
        </ul>
    </div>
</div>