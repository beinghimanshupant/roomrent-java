<%-- 
    Document   : aheader
    Created on : Feb 23, 2018, 7:46:50 PM
    Author     : Himanshu Pant
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- //for-mobile-apps -->
<link href="../files/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
<!-- pignose css -->
<link href="files/pignose.layerslider.css" rel="stylesheet" type="text/css" media="all" />


<!-- //pignose css -->
<link href="files/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script type="text/javascript" src="../files/bootstrap/js/jquery.min.js"></script>
<!-- //js -->
<!-- for bootstrap working -->
<script type="text/javascript" src="../files/bootstrap/js/bootstrap.min.js"></script>
<!-- //for bootstrap working -->
<script src="js/jquery.easing.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>RoomRent Rental</title>
    </head>
    <body>
         <div class="header-bot">
	<div class="container">
		<div class="col-md-3">
                    <h1><a href="sellerpanel.jsp">RoomRent</a></h1>
		</div>
             <div class="col-sm-5 col-md-5">
                 <form class="navbar-form" role="search" style="width:auto;" action="search.jsp">
           
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search" name="search" size="40" >
            <span class="input-group-btn">
                <button class="btn btn-success" type="submit">Go!</button>
                    </span>
        </div>
           
        </form>
    </div>
		
            
        
		<div style="text-align: right">Rental Panel
                    <% if(session.getAttribute("selleremail")!=null)
                    {%>
                    <br>
                    <a href="../logout.jsp" > <button class="btn-danger">Log out</button> </a>
                    <% } %>
                </div>
               
	</div>
</div>
<!-- //header-bot -->
<!-- banner -->


    </body>
</html>
