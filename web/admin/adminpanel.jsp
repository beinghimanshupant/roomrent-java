<%-- 
    Document   : adminpanel
    Created on : Feb 23, 2018, 7:17:43 PM
    Author     : Himanshu Pant
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
 String es = "";
if(session.getAttribute("adminemail")==null){
response.sendRedirect("alogin.jsp");
es = "none";
}
else{
es= session.getAttribute("adminemail").toString();
}
%>
<jsp:include page="aheader.jsp"/>

<!------ Include the above in your HEAD tag ---------->
<link href="../files/sellerpanel.css" rel="stylesheet" type="text/css"/>
<div class="container">
	<div class="row">
           	<div class="col-md-4">
                    <a href="managepost.jsp">
			<div class="dash-box dash-box-color-1">
				<div class="dash-box-icon">
					<i class="glyphicon"></i>
				</div>
				<div class="dash-box-body">
					<span class="dash-box-count"></span>
                                        <span class="dash-box-title"> <h2> Manage New Posts </h2> </span>
				</div>
				
				<div class="dash-box-action">
		
				</div>				
			</div>
                     </a>
		</div>
		<div class="col-md-4">
                    <a href="manageindex.jsp">
			<div class="dash-box dash-box-color-2">
				<div class="dash-box-icon">
					<i class="glyphicon mail-dropdown"></i>
				</div>
				<div class="dash-box-body">
				
                                    <span class="dash-box-title"><h2> Manage Homepage</h2></span>
				</div>
				
							
			</div>
                    </a>
		</div>
                <%
                    if(es.equals("admin@gmail.com")){%>
		<div class="col-md-4">
                    <a href="adminregister.jsp">
			<div class="dash-box dash-box-color-3">
				<div class="dash-box-icon">
					<i class="glyphicon mail-box"></i>
				</div>
				<div class="dash-box-body">
				
                                        <span class="dash-box-title"><h2>Add Admins</h2></span>
				</div>				
			</div>
                    </a>
		</div>
                <% }%>
            <div class="col-md-4">
                <a href="allpost.jsp">
			<div class="dash-box dash-box-color-1">
				<div class="dash-box-icon">
					<i class="glyphicon"></i>
				</div>
				<div class="dash-box-body">
					<span class="dash-box-count"></span>
                                        <span class="dash-box-title"> <h2> Manage All Posts </h2> </span>
				</div>
				
				<div class="dash-box-action">
		
				</div>				
			</div>
                     </a>
		</div>
            <div class="col-md-4">
                    <a href="contactmessage.jsp">
			<div class="dash-box dash-box-color-2">
				<div class="dash-box-icon">
					<i class="glyphicon mail-dropdown"></i>
				</div>
				<div class="dash-box-body">
				
                                    <span class="dash-box-title"><h2> Messages</h2></span>
				</div>
				
							
			</div>
                    </a>
		</div>
            <div class="col-md-4">
                    <a href="verifyusers.jsp">
			<div class="dash-box dash-box-color-3">
				<div class="dash-box-icon">
					<i class="glyphicon mail-box"></i>
				</div>
				<div class="dash-box-body">
				
                                        <span class="dash-box-title"><h2>Verify Users</h2></span>
				</div>				
			</div>
                    </a>
		</div>
             <div class="col-md-4">
                 <a href="amodifypassword.jsp">
			<div class="dash-box dash-box-color-1">
				<div class="dash-box-icon">
					<i class="glyphicon"></i>
				</div>
				<div class="dash-box-body">
					<span class="dash-box-count"></span>
                                        <span class="dash-box-title"> <h2> Change Password </h2> </span>
				</div>
				
				<div class="dash-box-action">
		
				</div>				
			</div>
                     </a>
		</div>
	</div>
    <a href="../index.jsp" >RoomRent </a>
</div>

