<%-- 
    Document   : verifyemail
    Created on : Feb 28, 2018, 4:01:18 PM
    Author     : Himanshu Pant
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<link rel="stylesheet" type="text/css" href="files/main.css">
<link href="files/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="files/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="files/bootstrap/js/bootstrap.min.js"></script>
<script src="files/bootstrap/js/jquery.min.js"></script>
		<script type="text/javascript">
    function confirmPass() {
        var pass = document.getElementById("password").value;
        var confPass = document.getElementById("confirm").value;
        if(pass != confPass) {
            alert('Wrong confirm password !');
            return false;
        }
    }
</script>
<%
    String email=request.getParameter("email");
    
    
    %>
Thank you for verifying your account.
You are our precious Renter and we cares about you.
Now you can edit your post after displaying on the RoomRent, by using your password.Create your password
<form class="form-horizontal" action="sellerupdate.jsp"  method="post" onsubmit="return confirmPass()">
    <input type="hidden" name="email" value="<%=email%>">
   
    				<div class="main-login main-center">
                                     <div class="form-group">
							<label for="name" class="cols-sm-2 control-label"> Name</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="name" id="name" maxlength="15" required placeholder="Enter your your"/>
								</div>
							</div>
						</div>

 <div class="form-group">
							<label for="password" class="cols-sm-2 control-label"> New Password</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="password" id="password" maxlength="15" required placeholder="Enter your Password"pattern=".{6,}" title="6 characters minimum"/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="confirm" class="cols-sm-2 control-label">Confirm New Password</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="confirm" id="confirm"  placeholder="Confirm your Password"/>
								</div>
							</div>
						</div>
           <div class="form-group ">
                                                    <input type="submit" class="btn btn-primary btn-lg btn-block login-button"value="Proceed"/>
						</div>
            </div>
</form>