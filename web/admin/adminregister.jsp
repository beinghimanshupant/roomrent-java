
<%@include  file="aheader.jsp" %>
<link href="../files/main.css" rel="stylesheet" type="text/css"/>
<div class="container">
			<div class="row main">
				<div class="panel-heading">
	               <div class="panel-title text-center">
	               		<h3 class="title">Welcome! Register Admin</h3>
                                
	               		<hr />
	               	</div>
	            </div> 
				<div class="main-login main-center">
                                    <form class="form-horizontal" method="post" action="admin_register_process.jsp">
                                        <input type="hidden" name="bname" value="<%=session.getAttribute("adminemail")%>"
						<h6>Enter the details of Admin</h6>
						<div class="form-group">
							<label for="name" class="cols-sm-2 control-label">Name</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                                                        <input type="text" class="form-control" name="name" maxlength="35" placeholder="Enter Name" required/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="email" class="cols-sm-2 control-label">Email</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                                                        <input type="email" class="form-control" name="email" maxlength="35" required  placeholder="Enter Email"/>
								</div>
							</div>
						</div>

					
						<div class="form-group">
							<label for="password" class="cols-sm-2 control-label">Password</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="password" id="password" maxlength="15" required placeholder="Enter your Password"/>
								</div>
							</div>
						</div>

					
						<div class="form-group ">
                                                    <input type="submit" class="btn btn-primary btn-lg btn-block login-button"value="Register"/>
						</div>
					
                      
				</div>
			</div>
		</div>