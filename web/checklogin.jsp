<%-- 
    Document   : checklogin
    Created on : Apr 24, 2018, 10:17:56 AM
    Author     : Himanshu Pant
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="header.jsp" %>
<link rel="stylesheet" type="text/css" href="files/main.css">
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="files/main.css">
    </head>
    <body>
     <div class="container">
			<div class="row main">
				<div class="panel-heading">
	               <div class="panel-title text-center">
	               		<h3 class="title">Welcome! Login </h3>
                                
	               		<hr />
	               	</div>
	            </div> 
				<div class="main-login main-center">
                                    <form class="form-horizontal" method="post" action="checkloginprocess.jsp">
						<h6>All fields are required</h6>
						<div class="form-group">
							<label for="email" class="cols-sm-2 control-label">Your Email</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                                                        <input type="email" class="form-control" name="email" maxlength="35" required  placeholder="Enter your Email"/>
								</div>
							</div>
						</div>
                                                <div class="form-group">
							<label for="password" class="cols-sm-2 control-label">Password</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="password" id="password" maxlength="15" required placeholder="Enter your Password"/>
								</div>
							</div>
                                                </div>
                                     <input type="submit" class="btn btn-primary btn-lg btn-block login-button"value="Login"/>
                                    </form>
                                    <div>
                                        <h4 ><center>  <a href="signup.jsp">Sign Up</a> </center></h4> 
                                        <p> Login using
                                    <%@include file="fblogin.jsp" %> 
                                    <p class="center-block"> Or</p>
                                    <%@include file="googlelogin.jsp" %>
                                    </div>
                                    </div>
     </div>
        
     </div>
				
    </body>
</html>
