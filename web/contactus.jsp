<%-- 
    Document   : contact
    Created on : Apr 28, 2018, 7:14:27 PM
    Author     : Himanshu Pant
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="header.jsp" %>
<div class="jumbotron jumbotron-sm">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <h1 class="h1">
                    Contact us <small>Feel free to contact us</small></h1>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="well well-sm">
                <form action="contactusprocess.jsp" method="post">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">
                                Name</label>
                            <input type="text" class="form-control" id="name" placeholder="Enter name" required="required" name="name" maxlength="30"/>
                        </div>
                        <div class="form-group">
                            <label for="mobile">
                                Mobile No:</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <input type="text" maxlength="10" class="form-control" id="mobile" pattern="^\d{10}$" placeholder="Enter 10 digit mobile number" required="required" name="mobile"/></div>
                        </div>
                        <div class="form-group">
                            <label for="subject">
                                Subject</label>
                            <select id="subject" name="subject" class="form-control" required="required" name="subject">
                                <option value="na" selected="">Choose One:</option>
                                <option value="service">General Customer Service</option>
                                <option value="suggestions">Suggestions</option>
                                <option value="product">Product Support</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">
                                Message</label>
                            <textarea name="message" id="message" class="form-control" rows="9" cols="25" required="required" maxlength="150"
                                placeholder="Message with max of 150 letters"></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary pull-right" id="btnContactUs">
                            Send Message</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <div class="col-md-4">
            <form>
            <legend><span class="glyphicon glyphicon-globe"></span> Our office</legend>
            <address>
                <strong>RoomRent</strong><br>
                Pauri<br>
               India<br>
                <abbr title="Phone">
                    P:</abbr>
               +919760012132
            </address>
            <address>
                <strong>RoomRent</strong><br>
                <a href="mailto:#">care.roomrent@gmail.com</a>
            </address>
            </form>
        </div>
    </div>
</div>
